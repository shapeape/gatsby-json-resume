# Gatsby JSON-Resume for GitLab Pages

[![pipeline status](https://gitlab.com/shapeape/gatsby-json-resume/badges/master/pipeline.svg)](https://gitlab.com/shapeape/gatsby-json-resume/commits/master)

Write your resume using JSON, deploy it using Gatsby and GitLab pages.

This is how it looks:  
![preview](preview.png)

Have a look at the deployed version [here](https://shapeape.gitlab.io/gatsby-json-resume).

## Get it running

- Clone this repo
- Install dependencies:

```
yarn
```

- Edit /resume/resume.json / anything, that you want to change.
- Test locally:

```
yarn start
```

Your local resume is here: http://localhost:8000/  
When finished:

- Login to GitLab
- New Project > Empty Project, no README
  You might use **resume** as the project name will be part of its url.

Back to your terminal:

```
git remote rename origin old-origin
git remote add origin https://gitlab.com/**yourusername**/**yourprojectname**.git
git push -u origin --all
git push -u origin --tags
```

- Commit, push your changes
- Watch GitLab pages deploy :)

## Based on

- [Gatsby](https://www.gatsbyjs.org/)

- [resume-with-gatsby](https://github.com/pcarion/resume-with-gatsby) by [Piere Carion](https://github.com/pcarion),  
   it uses YAML and deploys on Netlify, he even wrote an good [article](https://pcarion.com/article/gatsby-resume) about it.

- [GitLab pages Gatsby Template](https://gitlab.com/gitlab-org/project-templates/gatsby).  
  Get Gatsby running with CI/CD by GitLab Pages

- [JSON Resume](https://jsonresume.org/) A Project dedicated to writing resumes in JSON.

- [Flat](https://themes.jsonresume.org/theme/flat) JSON Resume theme by [Mattias Erming](https://github.com/erming)
