import React from "react";
import {
  Header,
  Contact,
  About,
  Profiles,
  Work,
  Volunteer,
  Education,
  Awards,
  Publications,
  Skills,
  Languages,
  Interests,
  References,
} from "./";

// console.log('@@ styles:', styles);

const Resume = ({ resume }) => {
  const {
    basics,
    work,
    volunteer,
    education,
    awards,
    publications,
    skills,
    languages,
    interests,
    references,
  } = resume;
  return (
    <React.Fragment>
      <Header basics={basics} />
      <div id="content" className="container">
        <Contact basics={basics} />
        <About basics={basics} />
        <Profiles profiles={basics.profiles || []} />
        <Work works={work} />
        <Volunteer volunteers={volunteer} />
        <Education educations={education} />
        <Awards awards={awards} />
        <Publications publications={publications} />
        <Skills skills={skills} />
        <Languages languages={languages} />
        <Interests interests={interests} />
        <References references={references} />
      </div>
    </React.Fragment>
  );
};

export default Resume;
